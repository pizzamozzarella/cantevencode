drop schema public cascade;
drop table user if exists;
create table user(
	id INT IDENTITY
,	name VARCHAR(20) NOT NULL
,	surname VARCHAR(20) NOT NULL
,	email VARCHAR(70) NOT NULL
,	password VARCHAR(255) NOT NULL
);

drop table follower if exists;
create table follower (
	id INT IDENTITY
,	follower_id INT NOT NULL
,	followed_id INT NOT NULL
,	FOREIGN KEY (follower_id) REFERENCES user(id)
,	FOREIGN KEY (followed_id) REFERENCES user(id)
);

drop table post if exists;
create table post(
	id INT IDENTITY
,	content VARCHAR(255) NOT NULL
,	user_id INT NOT NULL
,	FOREIGN KEY (user_id) REFERENCES user(id)
);