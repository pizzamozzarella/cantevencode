package cantevencode.dao;

import cantevencode.model.User;


public interface UserDao {
	public int getUserByEmail(String email);
	public void addUser(String name, String surname, String email, String password);
}
