package cantevencode.dao;

import java.util.*;

import cantevencode.model.Follower;
import cantevencode.model.Post;
import cantevencode.model.User;

public interface PostDao {
	public List<Post> getUserPosts(int user_id);
	public List<Post> getPrivatePosts(int user_id, Follower followed_id);
	public List<Post> getAllPosts();
	public void addPost(String content, int user_id);
}
