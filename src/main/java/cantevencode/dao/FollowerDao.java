package cantevencode.dao;

import cantevencode.model.User;
import cantevencode.model.Follower;

public interface FollowerDao {
	Follower startFollowing(User currentUser, User followed);
	void stopFollowing(User currentUser, User followed);
	boolean checkIfFollowed(User currentUser, User followed);
}
