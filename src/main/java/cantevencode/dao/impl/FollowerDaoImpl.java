package cantevencode.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import cantevencode.dao.FollowerDao;
import cantevencode.model.Follower;
import cantevencode.model.User;

@Transactional
public class FollowerDaoImpl implements FollowerDao{

	@PersistenceContext
	EntityManager entityManager;
	
	public Follower startFollowing(User currentUser, User followed) {
		if(!this.checkIfFollowed(currentUser, followed)) {
			String hql = "INSERT INTO Follower(follower_id) SELECT id FROM User WHERE id = :currentUser";
			Query query = entityManager.createQuery(hql)
					.setParameter("currentUser", currentUser.getUserId());
			query.executeUpdate();
			String hql2 = "UPDATE Follower SET followed_id = :followed WHERE follower_id = :currentUser AND id = MAX(id)";
			Query query2 = entityManager.createQuery(hql2)
					.setParameter("currentUser", currentUser.getUserId())
					.setParameter("followed", followed.getUserId());
			query.executeUpdate();
		}
		return null;
	}

	public void stopFollowing(User currentUser, User followed) {
		if(this.checkIfFollowed(currentUser, followed)) {
			Query relation_id = entityManager.createQuery("SELECT id FROM follower WHERE follower_id = :currentUser and followed_id = :followed");
			relation_id.setParameter("currentUser", currentUser);
			relation_id.setParameter("followed", followed);
			String hql = "DELETE FROM follower WHERE id = :relation_id";
			Query query = entityManager.createQuery(hql);
			query.setParameter("relation_id", relation_id);
			query.executeUpdate();
		}
	}

	public boolean checkIfFollowed(User currentUser, User followed) {
        String hql = "FROM Follower WHERE follower_id = :currentUser AND followed_id = :followed";
        Query query = entityManager.createQuery(hql)
        		.setParameter("currentUser", currentUser)
        		.setParameter("followed", followed);
		boolean isFollowing = false;
		try {
			if (query.getSingleResult() != null) {
				isFollowing = true;
			} else {
				isFollowing = false;
			}
        } catch (javax.persistence.NoResultException exception) {
            return false;
        }
		return isFollowing;
	}

}
