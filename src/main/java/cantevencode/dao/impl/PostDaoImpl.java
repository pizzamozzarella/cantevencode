package cantevencode.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import cantevencode.dao.PostDao;
import cantevencode.model.Post;
import cantevencode.model.User;
import cantevencode.model.Follower;

@Transactional
public class PostDaoImpl implements PostDao {

	@PersistenceContext
	EntityManager entityManager;
	
	public List<Post> getUserPosts(int user_id) {
		Query query = entityManager.createQuery("SELECT content FROM post where user_id = :user_id");
		query.setParameter("user_id", user_id);
		List<Post> user_posts = query.getResultList();
		return user_posts;
	}

	public List<Post> getPrivatePosts(int user_id, Follower followed_id) {
		Query query = entityManager.createQuery("SELECT followed_id FROM follower where user_id = :user_id");
		query.setParameter("user_id", user_id);
		List<Follower> followed = query.getResultList();
		Query query2 = entityManager.createQuery("SELECT content FROM post where user_id in (:user_id, :followed_ids)");
		query2.setParameter("followed_ids", followed);
		List<Post> private_posts = query2.getResultList();
		return private_posts;
	}

	public List<Post> getAllPosts() {
		Query query = entityManager.createQuery("SELECT content FROM post");
		List<Post> all_posts = query.getResultList();
		return all_posts;
	}

	public void addPost(String content, int user_id) {
		Post post = new Post();
		post.setAuthorId(user_id);
		post.setContent(content);
		entityManager.persist(post);
	}

}
