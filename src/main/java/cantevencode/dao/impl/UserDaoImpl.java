package cantevencode.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import cantevencode.dao.UserDao;
import cantevencode.model.User;

@Transactional
public class UserDaoImpl implements UserDao{
	
	@PersistenceContext
	EntityManager entityManager;

	public int getUserByEmail(String email) {
		Query query = entityManager.createQuery("SELECT id FROM user where email like ':email'");
		query.setParameter("email", email);
		return query.getFirstResult();
	}

	public void addUser(String name, String surname, String email, String password) {
		User user = new User();
		user.setUserName(name);
		user.setUserSurname(surname);
		user.setEmail(email);
		user.setPassword(password);
		entityManager.persist(user);
	}

}
