package cantevencode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {
	
	//annotations
	
	@Id
	@GeneratedValue
	@Column (name = "id", nullable = false)
	private int user_id;
	
	@Column (name = "name", nullable = false)
	private String name;
	
	@Column (name = "surname", nullable = false)
	private String surname;
	
	@Column (name = "email", nullable = false)
	private String email;
	
	@Column (name = "password", nullable = false)
	private String password;
	
	//setters and getters
	
	public int getUserId() {
		return this.user_id;
	}
	
	public void setUserId(int user_id) {
		this.user_id = user_id;
	}
	
	public String getUserName() {
		return this.name;
	}
	
	public void setUserName(String name) {
		this.name = name;
	}
	
	public String getUserSurname() {
		return this.surname;
	}
	
	public void setUserSurname(String surname) {
		this.surname = surname;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}
