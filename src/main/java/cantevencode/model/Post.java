package cantevencode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "post")
public class Post {
	
	//annotations
	
	@Id
	@GeneratedValue
	@Column (name = "id", nullable = false)
	private int post_id;
	
	@Column (name = "content")
	private String content;
	
	@Column (name = "user_id", nullable = false)
	private int author_id;
	
	//setters and getters
	
	public int getPostId() {
		return this.post_id;
	}
	
	public void setPostId(int post_id) {
		this.post_id = post_id;
	}
	
	public String getContent() {
		return this.content;
	}
	
	public void setContent(String content) {
		this.content = content;				
	}
	
	public int getAuthorId() {
		return this.author_id;
	}
	
	public void setAuthorId(int user_id) {
		this.author_id = user_id;
	}
}
