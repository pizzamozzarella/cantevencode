package cantevencode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "follower")
public class Follower {
	
	//annotations
	
	@Id
	@GeneratedValue
	@Column (name = "id", nullable = false)
	private int relation_id;
	
	@ManyToOne
	@JoinColumn (name = "follower_id", referencedColumnName = "id", nullable = false)
	private User follower_id;
	
	@ManyToOne
	@JoinColumn (name = "followed_id", referencedColumnName = "id", nullable = false)
	private User followed_id;
	
	//setters and getters
	
	public int getFollowerRelationId() {
		return this.relation_id;
	}
	
	public void setFollowerRelationId(int relation_id) {
		this.relation_id = relation_id;
	}
	
	public User getFollowerId() {
		return this.follower_id;
	}
	
	public Follower setFollowerId(User follower_id) {
		this.follower_id = follower_id;
		
		return this;
	}
	
	public User getFollowedId() {
		return this.followed_id;
	}
	
	public Follower setFollowedId(User followed_id) {
		this.followed_id = followed_id;
		
		return this;
	}
}
