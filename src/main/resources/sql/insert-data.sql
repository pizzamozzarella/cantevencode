INSERT INTO user(name, surname, email, password) VALUES
	('John', 'Doe', 'john.doe@gmail.com', 'johndoe123')
,	('Johnny', 'Donny', 'johnny.donny@gmail.com', 'johnny123')
,	('Joahna', 'Doana', 'joahna.doana@gmail.com', 'joahna123');

INSERT INTO follower(follower_id, followed_id) VALUES
 	(1, 2)
,	(1, 3)
,	(2, 1)
,	(2, 3)
,	(3, 1)
,	(3, 2);

INSERT INTO post(content, user_id) VALUES
	('Lorem ipsum 111', 1)
,	('Lorem ipsum 222', 1)
,	('LOREEM IPSUM ZZZZZZZZ', 1)
,	('Serious topic discussed here', 2)
,	('Not so serious topic here', 2)
,	('Not funny cat memes', 3);