package cantevencode.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import cantevencode.dao.FollowerDao;
import cantevencode.dao.UserDao;
import cantevencode.model.Follower;
import cantevencode.model.Post;
import cantevencode.model.User;

@RunWith (SpringJUnit4ClassRunner.class)
@ContextConfiguration (locations={"classpath:applicationContext-test.xml"})
@Transactional 
@Rollback(true)


public class TestFollowerDao {
	
	@Autowired
	FollowerDao followerDao;
	
	@Autowired
	UserDao userDao;
	
	//followed
	User newFollowed;
	
	//follower
	User newFollower;
	
	@Before
    public void setUp() {
		this.newFollower = new User();
		this.newFollower.setUserId(99);
		this.newFollower.setUserName("Mateusz");
		this.newFollower.setUserSurname("Dejniak");
		this.newFollower.setEmail("fajnymail123@mail.com");
		this.newFollower.setPassword("silnehaslo");
		
		this.newFollowed = new User();
		this.newFollowed.setUserId(100);
		this.newFollowed.setUserName("Bartek");
		this.newFollowed.setUserSurname("Golda");
		this.newFollowed.setEmail("Spokomail321@mail.com");
		this.newFollowed.setPassword("slabehaslo");
    }

    @Test
    public void testStartFollowing() {
    	
    	followerDao.startFollowing(newFollower, newFollowed);
    	
    	assertTrue(followerDao.checkIfFollowed(newFollower, newFollowed));
    }
}

