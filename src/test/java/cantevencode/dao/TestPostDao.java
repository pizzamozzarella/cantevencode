package cantevencode.dao;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import cantevencode.model.Post;
import cantevencode.dao.PostDao;
import cantevencode.dao.UserDao;
import cantevencode.model.User;
@RunWith (SpringJUnit4ClassRunner.class)
@ContextConfiguration (locations={"classpath:applicationContext-test.xml"})
@Transactional 
@Rollback(true)

@Component
public class TestPostDao {
	
	@PersistenceContext
	EntityManager em;
	
	@Autowired
	PostDao postDao;
	
	Post newPost;
	
	@Autowired
	UserDao userDao;
	
	@Before
	public void setUp() {
		this.newPost = new Post();
		this.newPost.setPostId(99);
		this.newPost.setContent("lorem ipsum");
		this.newPost.setAuthorId(99);
	}

	@Test
	public void testAddPost() {
		assertEquals( 99, newPost.getPostId());
		assertEquals( "lorem ipsum", newPost.getContent());
		assertEquals( 99, newPost.getAuthorId());
	}
}

