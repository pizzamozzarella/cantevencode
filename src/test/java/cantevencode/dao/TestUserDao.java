package cantevencode.dao;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import cantevencode.dao.UserDao;
import cantevencode.model.User;

@RunWith (SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext-test.xml"})
@Transactional 
@Rollback(true)


public class TestUserDao {
	
	@Autowired
	UserDao userDao;
	
	User newUser;
	
	@Before
	public void setUp() {
		this.newUser = new User();
		this.newUser.setUserId(99);
		this.newUser.setUserName("Janek");
		this.newUser.setUserSurname("Kowalski");
		this.newUser.setEmail("janek@test.com");
		this.newUser.setPassword("password123");	
	}
	
	@Test
	public void testAddUser() {
		assertEquals( "janek@test.com", newUser.getEmail());
		assertEquals( 99, newUser.getUserId());
		assertEquals("Janek", newUser.getUserName());
		assertEquals("Kowalski", newUser.getUserSurname());
		assertEquals("password123", newUser.getPassword());
	}
	
	@Test
	public void testGetUserByEmail() {
		assertNotNull(newUser.getEmail());
	}
}