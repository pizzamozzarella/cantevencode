package cantevencode.dao;
import org.junit.Test;

import cantevencode.model.MyUnit;

import static org.junit.Assert.*;

public class MyUnitTest {

	@Test
	public void testConcatenate() {
		MyUnit myUnit = new MyUnit();
		
		String result = myUnit.concatenate("one", "two");
		
		assertEquals("onetwo", result);
		
	}
}
